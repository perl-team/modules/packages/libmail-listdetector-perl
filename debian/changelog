libmail-listdetector-perl (1.04+dfsg-2) unstable; urgency=medium

  [ Debian Janitor ]
  * Trim trailing whitespace.
  * Bump debhelper from old 10 to 12.
  * Set debhelper-compat version in Build-Depends.

  [ gregor herrmann ]
  * debian/copyright: replace tabs with spaces / remove trailing
    whitespace.
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Bump debhelper from old 12 to 13.
  * Update standards version to 4.1.5, no changes needed.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Tue, 29 Nov 2022 20:05:40 +0000

libmail-listdetector-perl (1.04+dfsg-1) unstable; urgency=medium

  [ Jonathan Yu ]
  * New upstream release
  * Standards-Version 3.9.1 (no changes)

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ Salvatore Bonaccorso ]
  * debian/copyright: Replace DEP5 Format-Specification URL from
    svn.debian.org to anonscm.debian.org URL.

  [ gregor herrmann ]
  * debian/control: update {versioned,alternative} (build) dependencies.

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ gregor herrmann ]
  * Update debian/repack.stub.

  [ Axel Beckert ]
  * debian/copyright: migrate pre-1.0 format to 1.0 using "cme fix dpkg-
    copyright"

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.
  * Switch repackaging framework to Files-Excluded method.
  * Remove Jonathan Yu from Uploaders. Thanks for your work!

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * debian/copyright: add more upstream copyright holders.
  * Update years of packaging copyright.
  * debian/copyright: refresh license paragraphs.
  * Declare compliance with Debian Policy 4.1.4.
  * Bump debhelper compatibility level to 10.

 -- gregor herrmann <gregoa@debian.org>  Sat, 14 Apr 2018 21:53:24 +0200

libmail-listdetector-perl (1.03+dfsg-1) unstable; urgency=low

  [ Jonathan Yu ]
  * New upstream release
  * Use new short debhelper rules format
  * Add myself to Uploaders and Copyright
  * Install examples from upstream
  * Remove empty patches directory
  * Standards-Version 3.9.0 (no changes)
  * Bump compat level to 7
  * Update dependencies per upstream
  * Use new 3.0 (quilt) source format

  [ gregor herrmann ]
  * debian/control: Changed: Switched Vcs-Browser field to ViewSVN
    (source stanza).
  * Change my email address.
  * debian/control: remove unversioned (build) dependency on perl-
    modules.

  [ Nathan Handler ]
  * debian/watch: Update to ignore development releases.

  [ Salvatore Bonaccorso ]
  * debian/control: Changed: Replace versioned (build-)dependency on
    perl (>= 5.6.0-{12,16}) with an unversioned dependency on perl (as
    permitted by Debian Policy 3.8.3).

  [ Damyan Ivanov ]
  * bump required versions of libemail-abstract-perl and libemail-valid-perl as
    per upstream

 -- Jonathan Yu <jawnsy@cpan.org>  Mon, 12 Jul 2010 00:27:35 -0400

libmail-listdetector-perl (1.01+dfsg-3) unstable; urgency=low

  * Change the target directory of 'make install' to fix an empty package.
    (Closes: #470111).
  * Add myself to Uploaders.
  * Depend on ${misc:Depends}, as per debhelper(7).
  * Modify repack.sh to edit MANIFEST too, to match the current tarball
    in the archive and debian/copyright information.
  * Remove quilt framework, it's not needed anymore.

 -- Niko Tyni <ntyni@debian.org>  Sun, 09 Mar 2008 14:59:02 +0200

libmail-listdetector-perl (1.01+dfsg-2) unstable; urgency=low

  [ Joey Hess ]
  * Fix watch file to deal with mangled version number.

  [ gregor herrmann ]
  * debian/control: Added: Vcs-Svn field (source stanza); Vcs-Browser
    field (source stanza); Homepage field (source stanza). Removed: XS-
    Vcs-Svn fields.
  * debian/rules:
    - delete /usr/lib/perl5 only if it exists (closes: #467805)
    - use dh_listpackages for getting package name
    - introduce TMP variable for legibility
    - move dh_clean before make realclean and use it for removing stamp
      files
    - create install-stamp target
    - remove unneeded dh_* calls
  * Add repack script, use it in watch file and from a get-orig-source
    target in debian/rules.
  * Create a patch for removing t/* from MANIFEST; add quilt framework.
  * Set Standards-Version to 3.7.3 (no changes).
  * Remove debian/examples and install examples directly from debian/rules.
  * Remove debian/docs and install files directly from debian/rules.
  * debian/watch: use dist-based URL.
  * debian/copyright: mention actual download location instead of pointing
    to CPAN in general; add all upstream authors and assume that they are
    the copyright holders, too; take years of copyright from Changes.

 -- gregor herrmann <gregor+debian@comodo.priv.at>  Mon, 03 Mar 2008 20:37:34 +0100

libmail-listdetector-perl (1.01+dfsg-1) unstable; urgency=low

  * New upstream release.
  * debian/copyright: Change license information according to upstream
    changes and mention how the DFSG-free version was created.

 -- gregor herrmann <gregor+debian@comodo.priv.at>  Sat, 14 Apr 2007 00:18:14 +0200

libmail-listdetector-perl (0.34+dfsg-1) unstable; urgency=low

  * Uploading DFSG-free version of upstream version 0.34, excluding the
    non-free material in t/ (Closes: #404917)
  * Adopted by the Debian Pkg-perl Group (Closes: #357347)
  * Bumped up standards-version to 3.7.2.2
  * Moved debhelper from build-depends-indep to build-depends

 -- Gunnar Wolf <gwolf@debian.org>  Fri, 29 Dec 2006 00:26:14 -0600

libmail-listdetector-perl (0.34-1) unstable; urgency=low

  * QA upload.
  * New upstream release.

 -- Matej Vela <vela@debian.org>  Sat,  8 Apr 2006 22:40:20 +0200

libmail-listdetector-perl (0.32-1) unstable; urgency=low

  * QA upload.
  * New upstream release.
  * Package is orphaned (#357347); set maintainer to Debian QA Group.
  * lib/Mail/ListDetector/Detector/Lyris.pm: Restrict debugging output
    from appearing in normal usage.  Closes: #307800 [rt.cpan.org #18602].
  * Move sample.pl to /usr/share/doc/libmail-listdetector-perl/examples.
  * Remove unnecessary build dependencies on libemail-abstract-perl,
    libemail-valid-perl, liburi-perl.
  * Switch to debhelper 5.
  * Conforms to Standards version 3.6.2.

 -- Matej Vela <vela@debian.org>  Sat,  8 Apr 2006 14:22:20 +0200

libmail-listdetector-perl (0.31-1) unstable; urgency=medium

  * Initial Release. (Closes:Bug#259776)

 -- S. Zachariah Sprackett <zac@sprackett.com>  Thu, 15 Jul 2004 20:42:18 -0400
